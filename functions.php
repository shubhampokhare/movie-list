<?php
	include('db.php');
	include('customfunctions.php');
    $lang_filter = $_POST['lang_filter'];
    $gen_filter = $_POST['gen_filter'];
    $length_sort = $_POST['length_sort'];
    $date_sort = $_POST['date_sort'];
	$k = 0;
	$sql = "SELECT DISTINCT movies.id, movies.title, movies.description, movies.featured_image, movies.movie_length, movies.movie_release_date FROM movies JOIN relationship ON movies.id = relationship.movie_id JOIN category ON relationship.category_id = category.id";
	if($lang_filter != '0' || $gen_filter != 0){
		$sql .= ' WHERE category.id IN (';
		if($lang_filter != '0'){
			$sql .= "$lang_filter";
		}
		if($gen_filter != '0'){
			if($lang_filter != '0'){
				$sql .= ',';
				$k = 1;
			}
			$sql .= "$gen_filter";
		}
		$sql .= ')';
		if($k == 1){
			$sql .= " GROUP BY movies.id HAVING COUNT(*) = 2";
		}
	}
	if($length_sort != '0' || $date_sort != '0'){
		$sql .= ' ORDER BY ';
		if($length_sort != '0'){
			$sql .= "movies.movie_length $length_sort";
		}
		if($date_sort != '0'){
			$sql .= "CAST(movies.movie_release_date AS datetime) $date_sort";
		}	
	}

	$result2 = mysqli_query($conn, $sql); 
	$total_pages = ceil($result2->num_rows/10);
	if(isset($_POST['page_check'])){
		$page = $_POST['page'];
	} else{
		$page = 1;
	}
	
	$per_page = 10;
	$offset = ($page*$per_page)-$per_page;
	$sql .= " LIMIT ".$offset." , ".$per_page;
	$result = mysqli_query($conn, $sql);
	$html = "";
	$path = str_replace("functions.php","",$_SERVER['REQUEST_URI']);
	while($row = $result->fetch_assoc()) {
	$html .= "<tr>  
		<td>".$row['id']."</td>  
		<td>".$row['title']."</td>  
		<td>".$row['description']."</td>  
		<td>
			<img src=".$path.$row['featured_image']." alt='No image' width='50' height='50'>
		</td>  
		<td>".$row['movie_length']." minutes</td>  
		<td>";
			$date = new DateTime($row['movie_release_date']);
	$html .= $date->format('M d, Y')."
		</td> 
		<td>".get_filter('Language', $row['id'])."</td>
		<td>".get_filter('Genre', $row['id'])."</td>
	</tr>";
	}
	if($html == ''){
		$html = '<tr><td colspan="8">No results found</td></tr>';
		$page = 0;
		$total_pages = 0;
	}
	$paginat = '<li><a href="" class="first"><<</a></li>
			<li><a href="" class="previous"><</a></li>';
	$i = 1; 
	while($i<=$total_pages){
		if($i==$page){
			$active="active";
		}else{
			$active="";
		}
	$paginat .= '<li><a href=""  class="pagenumber '.$active.'">'.$i.'</a></li>';
		$i++; 
	}
	$paginat .= '<li><a href="" class="next">></a></li>
		<li><a href="" class="last">>></a></li>';
			
	echo json_encode(array('html'=>$html,'total_pages'=>$total_pages, 'paginat'=>$paginat, 'page'=>$page ));
?>