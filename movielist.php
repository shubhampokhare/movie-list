<?php 
	include('customfunctions.php');
?>
<div class="wrapper">
	<p class="head">List of movies</p>
	<div class="action">
		<div class="filter_by_lang">
			<label>Filter by Language : </label>
			<select id="lang-filter">
				<option value="0">Select</option>
				<?php 
					$filter = filterby('Language');
					foreach($filter as $key=>$val){
				?>
				<option value="<?php echo $key; ?>"><?php echo $val; ?></option>
				<?php
					}
				?>
			</select>
		</div>
		<div class="filter_by_gen">
			<label>Filter by Genre : </label>
			<select id="gen-filter">
				<option value="0">Select</option>
				<?php 
					$filter = filterby('Genre');
					foreach($filter as $key=>$val){
				?>
				<option value="<?php echo $key; ?>"><?php echo $val; ?></option>
				<?php
					}
				?>
			</select>
		</div>
		<div class="sort_by_length">
			<label>Sort by Length : </label>
			<select id="length-sort">
				<option value="0">Select</option>
				<option value="ASC">Asc</option>
				<option value="DESC">Desc</option>
			</select>
		</div>
		<div class="sort_by_date">
			<label>Sort by Release date : </label>
			<select id="date-sort">
				<option value="0">Select</option>
				<option value="ASC">Asc</option>
				<option value="DESC">Desc</option>
			</select>
		</div>
	</div>
	<?php
		$sql = "SELECT * FROM movies LIMIT 0,10";  
		$result = mysqli_query($conn, $sql); 
		$i = 0;

		$sql2 = "SELECT count(*) as cnt FROM movies";
		$result2 = mysqli_fetch_assoc(mysqli_query($conn, $sql2)); 
		$total_pages = ceil($result2['cnt']/10);
		

	?>
		<table id="movie-table" class="table table-bordered table-striped">  
		<thead>  
			<tr>  
				<th>ID</th>  
				<th>Title</th>  
				<th>Description</th>  
				<th>Featured Image</th>  
				<th>Movie Length</th>  
				<th>Movie Release Date</th>  
				<th>Language</th>
				<th>Genre</th>
			</tr>  
		</thead>  
		<tbody>  
		<?php  
		while($row = $result->fetch_assoc()) {
		?>  
			<tr>  
				<td><?php echo $row['id']; ?></td>  
				<td><?php echo $row['title']; ?></td>  
				<td><?php echo $row['description']; ?></td>  
				<td>
					<img src="<?php echo $_SERVER['REQUEST_URI'].$row['featured_image']; ?>" alt="No image" width="50" height="50">
				</td>  
				<td><?php echo $row['movie_length'].' minutes'; ?></td>  
				<td>
					<?php 
						$date = new DateTime($row['movie_release_date']);
						echo $date->format('M d, Y'); ?>
				</td> 
				<td><?php echo get_filter('Language', $row['id']); ?></td>
				<td><?php echo get_filter('Genre', $row['id']); ?></td>
			</tr>  
		<?php  
		}
		?>  
		</tbody>  
	</table>
	<div class="page_section">
		<ul class="pagination">
			<li><a href="" class="first"><<</a></li>
			<li><a href="" class="previous"><</a></li>
			<?php 
				$i = 1; 
				while($i<=$total_pages){
					if($i==1){
						$active=" active";
					}else{
						$active="";
					}
			?>
			<li><a href=""  class="pagenumber <?php echo $active;?>"><?php echo $i; ?></a></li>
				<?php $i++; } ?>
			<li><a href="" class="next">></a></li>
			<li><a href="" class="last">>></a></li>
		</ul>
		<p>Page <span class="page_currrent">1</span> of <span class="page_count"><?php echo $total_pages; ?></span></p>
	</div>
</div>

<script>
	$(document).ready(function(){
		$(document).on('change', '#lang-filter, #gen-filter, #length-sort, #date-sort, .page_click', function(e){
			e.preventDefault();
			if($(this).attr('id') == 'length-sort'){
				$('#date-sort').val(0);
			} else if($(this).attr('id') == 'date-sort'){
				$('#length-sort').val(0);
			}
			var data = { 
				'lang_filter' : $('#lang-filter').val(),
				'gen_filter' : $('#gen-filter').val(),
				'length_sort' : $('#length-sort').val(),
				'date_sort' : $('#date-sort').val(),
				'page': $('.pagenumber.active').text(),
				'page_check': window.page,
			};

			$.ajax({ 
				type      : 'POST',
				url       : 'functions.php',
				data      : data,
				success   : function(data) {
					$('#movie-table tbody').html('');
					var myJSON = $.parseJSON(data);			
					$('#movie-table tbody').html(myJSON['html']);
					$('.pagination').html(myJSON['paginat']);
					$('.page_currrent').text(myJSON['page']);
					$('.page_count').text(myJSON['total_pages']);
				}
			});
			delete window.page;
		});


		$(document).on('click','.pagenumber',function(e){
			e.preventDefault();
			$('.pagenumber').removeClass('active');
			$(this).addClass('active');
			window.page = 1;
			$('#lang-filter').trigger('change');
			
		});
		
		$(document).on('click','.next',function(e){
			e.preventDefault();
			var $next= $('.pagenumber.active').closest("li").next("li").children('.pagenumber');
			if($next.length==0){
				return false;
			}
			window.page = 1;
			$('.pagenumber').removeClass('active');
			$next.addClass('active');
			$('#lang-filter').trigger('change');

		});
		$(document).on('click','.previous',function(e){
			e.preventDefault();
			var $prev= $('.pagenumber.active').closest("li").prev("li").children('.pagenumber');
			if($prev.length==0){
				return false;
			}
			window.page = 1;
			$('.pagenumber').removeClass('active');
			$prev.addClass('active');
			$('#lang-filter').trigger('change');

		});

		$(document).on('click','.first',function(e){
			e.preventDefault();
			var $prev= $('.pagenumber.active').closest("li").prev("li").children('.pagenumber');
			if($prev.length==0){
				return false;
			}
			window.page = 1;
			$('.pagenumber').removeClass('active');
			$('.pagenumber:first').addClass('active');
			$('#lang-filter').trigger('change');

		});
		$(document).on('click','.last',function(e){
			e.preventDefault();
			var $next= $('.pagenumber.active').closest("li").next("li").children('.pagenumber');
			if($next.length==0){
				return false;
			}
			window.page = 1;
			$('.pagenumber').removeClass('active');
			$('.pagenumber:last').addClass('active');
			$('#lang-filter').trigger('change');

		});

		
	});
</script>